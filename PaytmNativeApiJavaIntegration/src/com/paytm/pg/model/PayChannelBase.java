package com.paytm.pg.model;

import java.util.List;

public class PayChannelBase {

	private String payMethod;
	private String payChannelOption;
	private StatusInfo isDisabled;
	private StatusInfo hasLowSuccess;
	private String iconUrl;
	private List<String> directServiceInsts;
	private List<String> supportAtmPins;

	public PayChannelBase() {
	}

	public PayChannelBase(String payMethod, String payChannelOption, StatusInfo isDisabled, StatusInfo hasLowSuccess,
			String iconUrl) {
		this.payMethod = payMethod;
		this.payChannelOption = payChannelOption;
		this.isDisabled = isDisabled;
		this.hasLowSuccess = hasLowSuccess;
		this.iconUrl = iconUrl;
	}

	public PayChannelBase(String payMethod, String payChannelOption, StatusInfo isDisabled, StatusInfo hasLowSuccess,
			String iconUrl, List<String> directServiceInsts, List<String> supportAtmPins) {
		super();
		this.payMethod = payMethod;
		this.payChannelOption = payChannelOption;
		this.isDisabled = isDisabled;
		this.hasLowSuccess = hasLowSuccess;
		this.iconUrl = iconUrl;
		this.directServiceInsts = directServiceInsts;
		this.supportAtmPins = supportAtmPins;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getPayChannelOption() {
		return payChannelOption;
	}

	public void setPayChannelOption(String payChannelOption) {
		this.payChannelOption = payChannelOption;
	}

	public StatusInfo getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(StatusInfo isDisabled) {
		this.isDisabled = isDisabled;
	}

	public StatusInfo getHasLowSuccess() {
		return hasLowSuccess;
	}

	public void setHasLowSuccess(StatusInfo hasLowSuccess) {
		this.hasLowSuccess = hasLowSuccess;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public List<String> getDirectServiceInsts() {
		return directServiceInsts;
	}

	public void setDirectServiceInsts(List<String> directServiceInsts) {
		this.directServiceInsts = directServiceInsts;
	}

	public List<String> getSupportAtmPins() {
		return supportAtmPins;
	}

	public void setSupportAtmPins(List<String> supportAtmPins) {
		this.supportAtmPins = supportAtmPins;
	}

	@Override
	public String toString() {
		return "PayChannelBase [payMethod=" + payMethod + ", payChannelOption=" + payChannelOption + ", isDisabled="
				+ isDisabled + ", hasLowSuccess=" + hasLowSuccess + ", iconUrl=" + iconUrl + ", directServiceInsts="
				+ directServiceInsts + ", supportAtmPins=" + supportAtmPins + ", toString()=" + super.toString() + "]";
	}

}
