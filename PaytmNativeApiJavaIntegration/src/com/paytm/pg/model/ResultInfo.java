package com.paytm.pg.model;

public class ResultInfo {
	private String resultStatus;
	private String resultCode;
	private String resultMsg;
	private Boolean isRedirect;

	public ResultInfo() {
		super();
	}

	public ResultInfo(String resultStatus, String resultCode, String resultMsg) {
		super();
		this.resultStatus = resultStatus;
		this.resultCode = resultCode;
		this.resultMsg = resultMsg;
	}

	public ResultInfo(String resultStatus, String resultCode, String resultMsg, Boolean isRedirect) {
		super();
		this.resultStatus = resultStatus;
		this.resultCode = resultCode;
		this.resultMsg = resultMsg;
		this.isRedirect = isRedirect;
	}

	public String getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public Boolean isRedirect() {
		return isRedirect;
	}

	public void setRedirect(Boolean isRedirect) {
		this.isRedirect = isRedirect;
	}

	@Override
	public String toString() {
		return "ResultInfo [resultStatus=" + resultStatus + ", resultCode=" + resultCode + ", resultMsg=" + resultMsg
				+ ", isRedirect=" + isRedirect + ", toString()=" + super.toString() + "]";
	}

}
