package com.paytm.pg.model;

public class GoodsInfo {

	private String merchantGoodsId;
	private String merchantShippingId;
	private String snapshotUrl;
	private String description;
	private String category;
	private String quantity;
	private String unit;
	private Money price;
	private ExtendInfo extendInfo;

	public GoodsInfo(String merchantGoodsId, String merchantShippingId, String snapshotUrl, String description,
			String category, String quantity, String unit, Money price, ExtendInfo extendInfo) {
		super();
		this.merchantGoodsId = merchantGoodsId;
		this.merchantShippingId = merchantShippingId;
		this.snapshotUrl = snapshotUrl;
		this.description = description;
		this.category = category;
		this.quantity = quantity;
		this.unit = unit;
		this.price = price;
		this.extendInfo = extendInfo;
	}

	public String getMerchantGoodsId() {
		return merchantGoodsId;
	}

	public void setMerchantGoodsId(String merchantGoodsId) {
		this.merchantGoodsId = merchantGoodsId;
	}

	public String getMerchantShippingId() {
		return merchantShippingId;
	}

	public void setMerchantShippingId(String merchantShippingId) {
		this.merchantShippingId = merchantShippingId;
	}

	public String getSnapshotUrl() {
		return snapshotUrl;
	}

	public void setSnapshotUrl(String snapshotUrl) {
		this.snapshotUrl = snapshotUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Money getPrice() {
		return price;
	}

	public void setPrice(Money price) {
		this.price = price;
	}

	public ExtendInfo getExtendInfo() {
		return extendInfo;
	}

	public void setExtendInfo(ExtendInfo extendInfo) {
		this.extendInfo = extendInfo;
	}

	@Override
	public String toString() {
		return "GoodsInfo [merchantGoodsId=" + merchantGoodsId + ", merchantShippingId=" + merchantShippingId
				+ ", snapshotUrl=" + snapshotUrl + ", description=" + description + ", category=" + category
				+ ", quantity=" + quantity + ", unit=" + unit + ", price=" + price + ", extendInfo=" + extendInfo
				+ ", toString()=" + super.toString() + "]";
	}

}
