package com.paytm.pg.model;

public class ShippingInfo {

	private String merchantShippingId;
	private String trackingNo;
	private String carrier;
	private Money chargeAmount;
	private String countryName;
	private String stateName;
	private String cityName;
	private String address1;
	private String address2;
	private String firstName;
	private String lastName;
	private String mobileNo;
	private String zipCode;
	private String email;

	public ShippingInfo(String merchantShippingId, String trackingNo, String carrier, Money chargeAmount,
			String countryName, String stateName, String cityName, String address1, String address2, String firstName,
			String lastName, String mobileNo, String zipCode, String email) {
		super();
		this.merchantShippingId = merchantShippingId;
		this.trackingNo = trackingNo;
		this.carrier = carrier;
		this.chargeAmount = chargeAmount;
		this.countryName = countryName;
		this.stateName = stateName;
		this.cityName = cityName;
		this.address1 = address1;
		this.address2 = address2;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNo = mobileNo;
		this.zipCode = zipCode;
		this.email = email;
	}

	public String getMerchantShippingId() {
		return merchantShippingId;
	}

	public void setMerchantShippingId(String merchantShippingId) {
		this.merchantShippingId = merchantShippingId;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public Money getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(Money chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ShippingInfo [merchantShippingId=" + merchantShippingId + ", trackingNo=" + trackingNo + ", carrier="
				+ carrier + ", chargeAmount=" + chargeAmount + ", countryName=" + countryName + ", stateName="
				+ stateName + ", cityName=" + cityName + ", address1=" + address1 + ", address2=" + address2
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", mobileNo=" + mobileNo + ", zipCode="
				+ zipCode + ", email=" + email + ", toString()=" + super.toString() + "]";
	}

}
