package com.paytm.pg.model;

import java.util.List;

public class PayMethod {

	private String paymentMode;
	private String displayName;
	private StatusInfo isDisabled;
	private List<PayChannelBase> payChannelOptions;

	public PayMethod() {
	}

	public PayMethod(String payMethod, String displayName, StatusInfo isDisabled,
			List<PayChannelBase> payChannelOptions) {
		super();
		this.paymentMode = payMethod;
		this.displayName = displayName;
		this.isDisabled = isDisabled;
		this.payChannelOptions = payChannelOptions;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public StatusInfo getIsDisabled() {
		return this.isDisabled;
	}

	public List<PayChannelBase> getPayChannelOptions() {
		return this.payChannelOptions;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setIsDisabled(StatusInfo isDisabled) {
		this.isDisabled = isDisabled;
	}

	public void setPayChannelOptions(List<PayChannelBase> payChannelOptions) {
		this.payChannelOptions = payChannelOptions;
	}

	@Override
	public String toString() {
		return "PayMethod [paymentMode=" + paymentMode + ", displayName=" + displayName + ", isDisabled=" + isDisabled
				+ ", payChannelOptions=" + payChannelOptions + ", toString()=" + super.toString() + "]";
	}

}
