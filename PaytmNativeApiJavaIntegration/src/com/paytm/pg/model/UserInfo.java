package com.paytm.pg.model;

public class UserInfo {

	private String custId;
	private String mobile;
	private String email;
	private String firstName;
	private String lastName;

	public UserInfo(String custId, String mobile, String email, String firstName, String lastName) {
		this.custId = custId;
		this.mobile = mobile;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "UserInfo [custId=" + custId + ", mobile=" + mobile + ", email=" + email + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", toString()=" + super.toString() + "]";
	}

}
