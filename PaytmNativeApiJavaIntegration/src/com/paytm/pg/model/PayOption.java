package com.paytm.pg.model;

import java.util.List;

public class PayOption {
	private List<PayMethod> paymentModes = null;
	private List<PayChannelBase> savedInstruments = null;

	public PayOption() {
		super();
	}

	public PayOption(List<PayMethod> paymentModes) {
		super();
		this.paymentModes = paymentModes;
	}

	public PayOption(List<PayMethod> payMethods, List<PayChannelBase> savedInstruments) {
		super();
		this.paymentModes = payMethods;
		this.savedInstruments = savedInstruments;
	}

	public List<PayMethod> getPaymentModes() {
		return paymentModes;
	}

	public void setPaymentModes(List<PayMethod> paymentModes) {
		this.paymentModes = paymentModes;
	}

	public List<PayChannelBase> getSavedInstruments() {
		return savedInstruments;
	}

	public void setSavedInstruments(List<PayChannelBase> savedInstruments) {
		this.savedInstruments = savedInstruments;
	}

	@Override
	public String toString() {
		return "PayOption [paymentModes=" + paymentModes + ", savedInstruments=" + savedInstruments + ", toString()="
				+ super.toString() + "]";
	}

}
