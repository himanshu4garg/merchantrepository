package com.paytm.pg.model;

public class NativePromoCodeData {

	private String promoCode;
	private String promoMsg;
	private boolean promoCodeValid;
	private String promoCodeTypeName;
	private String promoCodeMsg;

	public NativePromoCodeData() {
	}

	public NativePromoCodeData(String promoCode, String promoMsg, boolean promoCodeValid, String promoCodeTypeName,
			String promoCodeMsg) {
		super();
		this.promoCode = promoCode;
		this.promoMsg = promoMsg;
		this.promoCodeValid = promoCodeValid;
		this.promoCodeTypeName = promoCodeTypeName;
		this.promoCodeMsg = promoCodeMsg;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getPromoMsg() {
		return promoMsg;
	}

	public void setPromoMsg(String promoMsg) {
		this.promoMsg = promoMsg;
	}

	public boolean isPromoCodeValid() {
		return promoCodeValid;
	}

	public void setPromoCodeValid(boolean promoCodeValid) {
		this.promoCodeValid = promoCodeValid;
	}

	public String getPromoCodeTypeName() {
		return promoCodeTypeName;
	}

	public void setPromoCodeTypeName(String promoCodeTypeName) {
		this.promoCodeTypeName = promoCodeTypeName;
	}

	public String getPromoCodeMsg() {
		return promoCodeMsg;
	}

	public void setPromoCodeMsg(String promoCodeMsg) {
		this.promoCodeMsg = promoCodeMsg;
	}

	@Override
	public String toString() {
		return "NativePromoCodeData [promoCode=" + promoCode + ", promoMsg=" + promoMsg + ", promoCodeValid="
				+ promoCodeValid + ", promoCodeTypeName=" + promoCodeTypeName + ", promoCodeMsg=" + promoCodeMsg
				+ ", toString()=" + super.toString() + "]";
	}

}
