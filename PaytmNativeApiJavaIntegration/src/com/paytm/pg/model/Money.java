package com.paytm.pg.model;

public class Money {

	private String currency;
	private String value;

	public Money() {
	}

	public Money(String value) {
		this.currency = "INR";
		this.value = value;
	}

	public Money(String currency, String value) {
		this.currency = currency;
		this.value = value;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Money [currency=" + currency + ", value=" + value + ", toString()=" + super.toString() + "]";
	}

}
