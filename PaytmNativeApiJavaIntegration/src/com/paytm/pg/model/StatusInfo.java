package com.paytm.pg.model;

public class StatusInfo {

	private String status;
	private String msg;

	public StatusInfo(String status, String msg) {
		this.status = status;
		this.msg = msg;
	}

	public StatusInfo() {
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "StatusInfo [status=" + status + ", msg=" + msg + ", toString()=" + super.toString() + "]";
	}

}
