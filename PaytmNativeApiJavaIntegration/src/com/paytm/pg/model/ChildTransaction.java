package com.paytm.pg.model;

import com.google.gson.annotations.SerializedName;

public class ChildTransaction {
	public static final String TXNID = "TXNID";
	public static final String PAYMENTMODE = "PAYMENTMODE";
	public static final String TXNAMOUNT = "TXNAMOUNT";
	public static final String GATEWAYNAME = "GATEWAYNAME";
	public static final String BANKTXNID = "BANKTXNID";
	public static final String BANKNAME = "BANKNAME";
	public static final String STATUS = "STATUS";
	public static final String MASKED_CARD_NO = "maskedCardNo";
	public static final String CARD_INDEX_NO = "cardIndexNo";

	@SerializedName(TXNID)
	private String txnId = "";

	@SerializedName(PAYMENTMODE)
	private String paymentMode = "";

	@SerializedName(TXNAMOUNT)
	private String txnAmount = "";

	@SerializedName(GATEWAYNAME)
	private String gateway = "";

	@SerializedName(BANKTXNID)
	private String bankTxnId = "";

	@SerializedName(BANKNAME)
	private String bankName = "";

	@SerializedName(STATUS)
	private String status = "";

	@SerializedName(CARD_INDEX_NO)
	private String cardIndexNo = "";

	@SerializedName(MASKED_CARD_NO)
	private String maskedCardNo = "";

	public ChildTransaction(String txnId, String paymentMode, String txnAmount, String gateway, String bankTxnId,
			String bankName, String status, String cardIndexNo, String maskedCardNo) {
		super();
		this.txnId = txnId;
		this.paymentMode = paymentMode;
		this.txnAmount = txnAmount;
		this.gateway = gateway;
		this.bankTxnId = bankTxnId;
		this.bankName = bankName;
		this.status = status;
		this.cardIndexNo = cardIndexNo;
		this.maskedCardNo = maskedCardNo;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getBankTxnId() {
		return bankTxnId;
	}

	public void setBankTxnId(String bankTxnId) {
		this.bankTxnId = bankTxnId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCardIndexNo() {
		return cardIndexNo;
	}

	public void setCardIndexNo(String cardIndexNo) {
		this.cardIndexNo = cardIndexNo;
	}

	public String getMaskedCardNo() {
		return maskedCardNo;
	}

	public void setMaskedCardNo(String maskedCardNo) {
		this.maskedCardNo = maskedCardNo;
	}

	@Override
	public String toString() {
		return "ChildTransaction [txnId=" + txnId + ", paymentMode=" + paymentMode + ", txnAmount=" + txnAmount
				+ ", gateway=" + gateway + ", bankTxnId=" + bankTxnId + ", bankName=" + bankName + ", status=" + status
				+ ", cardIndexNo=" + cardIndexNo + ", maskedCardNo=" + maskedCardNo + ", toString()=" + super.toString()
				+ "]";
	}

}
