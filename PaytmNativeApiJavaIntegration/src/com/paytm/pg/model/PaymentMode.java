package com.paytm.pg.model;

import java.util.List;

public class PaymentMode {

	private String mode;
	private List<String> channels = null;

	public PaymentMode() {
	}

	public PaymentMode(String mode, List<String> channels) {
		super();
		this.mode = mode;
		this.channels = channels;
	}

	public PaymentMode(String mode) {
		this.mode = mode;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<String> getChannels() {
		return channels;
	}

	public void setChannels(List<String> channels) {
		this.channels = channels;
	}

	@Override
	public String toString() {
		return "PaymentMode [mode=" + mode + ", channels=" + channels + ", toString()=" + super.toString() + "]";
	}

}
