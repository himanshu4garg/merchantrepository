package com.paytm.pg.model;

public class MerchantDetails {
	private String mcc;
	private String merchantVpa;
	private String merchantName;

	public MerchantDetails(String mcc, String merchantVpa, String merchantName) {
		this.mcc = mcc;
		this.merchantVpa = merchantVpa;
		this.merchantName = merchantName;
	}

	public String getMcc() {
		return mcc;
	}

	public void setMcc(String mcc) {
		this.mcc = mcc;
	}

	public String getMerchantVpa() {
		return merchantVpa;
	}

	public void setMerchantVpa(String merchantVpa) {
		this.merchantVpa = merchantVpa;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	@Override
	public String toString() {
		return "MerchantDetails [mcc=" + mcc + ", merchantVpa=" + merchantVpa + ", merchantName=" + merchantName
				+ ", toString()=" + super.toString() + "]";
	}

}
