package com.paytm.pg.model;

import java.util.Map;

public class ExtendInfo {
	private String udf1;
	private String udf2;
	private String udf3;
	private String mercUnqRef;
	private String comments;
	private String amountToBeRefunded;
	private Map<String, String> subwalletAmount;

	public ExtendInfo(String udf1, String udf2, String udf3, String mercUnqRef, String comments,
			String amountToBeRefunded, Map<String, String> subwalletAmount) {
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.mercUnqRef = mercUnqRef;
		this.comments = comments;
		this.amountToBeRefunded = amountToBeRefunded;
		this.subwalletAmount = subwalletAmount;
	}

	public void setSubwalletAmount(Map<String, String> subwalletAmount) {
		this.subwalletAmount = subwalletAmount;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getMercUnqRef() {
		return mercUnqRef;
	}

	public void setMercUnqRef(String mercUnqRef) {
		this.mercUnqRef = mercUnqRef;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getAmountToBeRefunded() {
		return amountToBeRefunded;
	}

	public Map<String, String> getSubwalletAmount() {
		return subwalletAmount;
	}

	public void setAmountToBeRefunded(String amountToBeRefunded) {
		this.amountToBeRefunded = amountToBeRefunded;
	}

	@Override
	public String toString() {
		return "ExtendInfo [udf1=" + udf1 + ", udf2=" + udf2 + ", udf3=" + udf3 + ", mercUnqRef=" + mercUnqRef
				+ ", comments=" + comments + ", amountToBeRefunded=" + amountToBeRefunded + ", subwalletAmount="
				+ subwalletAmount + ", toString()=" + super.toString() + "]";
	}

}
