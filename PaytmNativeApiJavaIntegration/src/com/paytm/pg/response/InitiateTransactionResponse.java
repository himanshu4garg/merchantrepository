package com.paytm.pg.response;

import com.paytm.pg.model.ResultInfo;
import com.paytm.pg.response.body.InitiateTransactionResponseBody;
import com.paytm.pg.response.header.SecureResponseHeader;

public class InitiateTransactionResponse {

	private SecureResponseHeader head;
	private InitiateTransactionResponseBody body;

	public InitiateTransactionResponse() {
	}

	public InitiateTransactionResponse(SecureResponseHeader head, InitiateTransactionResponseBody body) {
		this.head = head;
		this.body = body;
	}

	public InitiateTransactionResponse(ResultInfo result) {
		this.head = new SecureResponseHeader();
		this.body = new InitiateTransactionResponseBody(result);
	}

	public SecureResponseHeader getHead() {
		return head;
	}

	public void setHead(SecureResponseHeader head) {
		this.head = head;
	}

	public InitiateTransactionResponseBody getBody() {
		return body;
	}

	public void setBody(InitiateTransactionResponseBody body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "InitiateTransactionResponse [head=" + head + ", body=" + body + ", toString()=" + super.toString()
				+ "]";
	}

}
