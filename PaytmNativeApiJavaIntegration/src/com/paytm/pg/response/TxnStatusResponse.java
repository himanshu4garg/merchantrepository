package com.paytm.pg.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.paytm.pg.model.ChildTransaction;

public class TxnStatusResponse {

	public static final String MID = "MID";
	public static final String ORDERID = "ORDERID";
	public static final String TXNTYPE = "TXNTYPE";

	public static final String TXNID = "TXNID";
	public static final String PAYMENTMODE = "PAYMENTMODE";
	public static final String TXNAMOUNT = "TXNAMOUNT";
	public static final String GATEWAYNAME = "GATEWAYNAME";
	public static final String BANKTXNID = "BANKTXNID";
	public static final String BANKNAME = "BANKNAME";
	public static final String STATUS = "STATUS";
	public static final String RESPMSG = "RESPMSG";
	public static final String RESPCODE = "RESPCODE";

	public static final String REFUNDAMT = "REFUNDAMT";
	public static final String TXNDATE = "TXNDATE";
	public static final String REFUNDID = "REFUNDID";
	public static final String REFID = "REFID";
	public static final String CHILDTXNLIST = "CHILDTXNLIST";
	public static final String MERC_UNQ_REF = "MERC_UNQ_REF";
	public static final String SUBS_ID = "SUBS_ID";
	public static final String PREAUTH_ID = "PREAUTH_ID";
	public static final String BLOCKEDAMOUNT = "BLOCKEDAMOUNT";
	public static final String MASKED_CARD_NO = "maskedCardNo";
	public static final String CARD_INDEX_NO = "cardIndexNo";

	public static final String MASKED_CUSTOMER_MOBILE_NUMBER = "MASKED_CUSTOMER_MOBILE_NUMBER";
	public static final String POS_ID = "POS_ID";
	public static final String UNIQUE_REFERENCE_LABEL = "UNIQUE_REFERENCE_LABEL";
	public static final String UNIQUE_REFERENCE_VALUE = "UNIQUE_REFERENCE_VALUE";
	public static final String PCC_CODE = "PCC_CODE";
	public static final String PRN = "PRN";
	public static final String UDF_1 = "UDF_1";
	public static final String UDF_2 = "UDF_2";
	public static final String UDF_3 = "UDF_3";
	public static final String COMMENTS = "COMMENTS";
	public static final String CURRENT_TXN_COUNT = "currentTxnCount";
	public static final String LOYALTYPOINTS = "LOYALTYPOINTS";

	@SerializedName(TXNID)
	private String txnId = "";

	@SerializedName(BANKTXNID)
	private String bankTxnId = "";

	@SerializedName(ORDERID)
	private String orderId = "";

	@SerializedName(TXNAMOUNT)
	private String txnAmount = "";

	@SerializedName(STATUS)
	private String status = "";

	@SerializedName(TXNTYPE)
	private String txnType = "";

	@SerializedName(GATEWAYNAME)
	private String gatewayName = "";

	@SerializedName(RESPCODE)
	private String respCode = "";

	@SerializedName(RESPMSG)
	private String respMsg = "";

	@SerializedName(BANKNAME)
	private String bankName = "";

	@SerializedName(MID)
	private String mid = "";

	@SerializedName(PAYMENTMODE)
	private String paymentMode = "";

	@SerializedName(REFUNDAMT)
	private String refundAmt = "";

	@SerializedName(TXNDATE)
	private String txnDate = "";

	@SerializedName(REFUNDID)
	private String refundId;

	@SerializedName(REFID)
	private String refId;

	@SerializedName(CHILDTXNLIST)
	private List<ChildTransaction> childTransaction;

	@SerializedName(SUBS_ID)
	private String subsId;

	@SerializedName(MERC_UNQ_REF)
	private String merchantUniqueReference;

	@SerializedName(BLOCKEDAMOUNT)
	private String blockedAmount;

	@SerializedName(PREAUTH_ID)
	private String preAuthId;

	private transient String customMerchantResponse;
	private transient String customChecksumString;

	@SerializedName(MASKED_CARD_NO)
	private String maskedCardNo;

	@SerializedName(CARD_INDEX_NO)
	private String cardIndexNo;

	@SerializedName(MASKED_CUSTOMER_MOBILE_NUMBER)
	private String maskedCustomerMobileNumber;
	@SerializedName(POS_ID)
	private String posId;
	@SerializedName(UNIQUE_REFERENCE_LABEL)
	private String uniqueReferenceLabel;
	@SerializedName(UNIQUE_REFERENCE_VALUE)
	private String uniqueReferenceValue;
	@SerializedName(PCC_CODE)
	private String pccCode;
	@SerializedName(PRN)
	private String prn;
	@SerializedName(UDF_1)
	private String udf1;
	@SerializedName(UDF_2)
	private String udf2;
	@SerializedName(UDF_3)
	private String udf3;
	@SerializedName(COMMENTS)
	private String comments;
	@SerializedName(CURRENT_TXN_COUNT)
	private String currentTxnCount;
	@SerializedName(LOYALTYPOINTS)
	private String loyaltyPoints;

	public TxnStatusResponse(String txnId, String bankTxnId, String orderId, String txnAmount, String status,
			String txnType, String gatewayName, String respCode, String respMsg, String bankName, String mid,
			String paymentMode, String refundAmt, String txnDate, String refundId, String refId,
			List<ChildTransaction> childTransaction, String subsId, String merchantUniqueReference,
			String blockedAmount, String preAuthId, String customMerchantResponse, String customChecksumString,
			String maskedCardNo, String cardIndexNo, String maskedCustomerMobileNumber, String posId,
			String uniqueReferenceLabel, String uniqueReferenceValue, String pccCode, String prn, String udf1,
			String udf2, String udf3, String comments, String currentTxnCount, String loyaltyPoints) {
		super();
		this.txnId = txnId;
		this.bankTxnId = bankTxnId;
		this.orderId = orderId;
		this.txnAmount = txnAmount;
		this.status = status;
		this.txnType = txnType;
		this.gatewayName = gatewayName;
		this.respCode = respCode;
		this.respMsg = respMsg;
		this.bankName = bankName;
		this.mid = mid;
		this.paymentMode = paymentMode;
		this.refundAmt = refundAmt;
		this.txnDate = txnDate;
		this.refundId = refundId;
		this.refId = refId;
		this.childTransaction = childTransaction;
		this.subsId = subsId;
		this.merchantUniqueReference = merchantUniqueReference;
		this.blockedAmount = blockedAmount;
		this.preAuthId = preAuthId;
		this.customMerchantResponse = customMerchantResponse;
		this.customChecksumString = customChecksumString;
		this.maskedCardNo = maskedCardNo;
		this.cardIndexNo = cardIndexNo;
		this.maskedCustomerMobileNumber = maskedCustomerMobileNumber;
		this.posId = posId;
		this.uniqueReferenceLabel = uniqueReferenceLabel;
		this.uniqueReferenceValue = uniqueReferenceValue;
		this.pccCode = pccCode;
		this.prn = prn;
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.comments = comments;
		this.currentTxnCount = currentTxnCount;
		this.loyaltyPoints = loyaltyPoints;
	}

	public String getMaskedCustomerMobileNumber() {
		return maskedCustomerMobileNumber;
	}

	public void setMaskedCustomerMobileNumber(String maskedCustomerMobileNumber) {
		this.maskedCustomerMobileNumber = maskedCustomerMobileNumber;
	}

	public String getPosId() {
		return posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	public String getLoyaltyPoints() {
		return loyaltyPoints;
	}

	public void setLoyaltyPoints(String loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

	public String getPrn() {
		return prn;
	}

	public void setPrn(String prn) {
		this.prn = prn;
	}

	public String getUniqueReferenceLabel() {
		return uniqueReferenceLabel;
	}

	public void setUniqueReferenceLabel(String uniqueReferenceLabel) {
		this.uniqueReferenceLabel = uniqueReferenceLabel;
	}

	public String getUniqueReferenceValue() {
		return uniqueReferenceValue;
	}

	public void setUniqueReferenceValue(String uniqueReferenceValue) {
		this.uniqueReferenceValue = uniqueReferenceValue;
	}

	public String getPccCode() {
		return pccCode;
	}

	public void setPccCode(String pccCode) {
		this.pccCode = pccCode;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public TxnStatusResponse(String respCode, String respMsg, String status, String mid, String orderId) {
		this.respCode = respCode;
		this.respMsg = respMsg;
		this.status = status;
		this.mid = mid;
		this.orderId = orderId;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getBankTxnId() {
		return bankTxnId;
	}

	public void setBankTxnId(String bankTxnId) {
		this.bankTxnId = bankTxnId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getGatewayName() {
		return gatewayName;
	}

	public void setGatewayName(String gatewayName) {
		this.gatewayName = gatewayName;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespMsg() {
		return respMsg;
	}

	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getRefundAmt() {
		return refundAmt;
	}

	public void setRefundAmt(String refundAmt) {
		this.refundAmt = refundAmt;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public String getRefundId() {
		return refundId;
	}

	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public List<ChildTransaction> getChildTransaction() {
		return childTransaction;
	}

	public void setChildTransaction(List<ChildTransaction> childTransaction) {
		this.childTransaction = childTransaction;
	}

	public String getCurrentTxnCount() {
		return currentTxnCount;
	}

	public void setCurrentTxnCount(String currentTxnCount) {
		this.currentTxnCount = currentTxnCount;
	}

	public TxnStatusResponse() {
	}

	public TxnStatusResponse(String respCode, String respMsg) {
		this.respCode = respCode;
		this.respMsg = respMsg;
		txnId = bankTxnId = orderId = txnAmount = status = txnType = gatewayName = bankName = mid = paymentMode = refundAmt = txnDate = refundId = refId = null;
	}


	/**
	 * @return the subsId
	 */
	public String getSubsId() {
		return subsId;
	}

	/**
	 * @param subsId the subsId to set
	 */
	public void setSubsId(String subsId) {
		this.subsId = subsId;
	}

	/**
	 * @return the merchantUniqueReference
	 */
	public String getMerchantUniqueReference() {
		return merchantUniqueReference;
	}

	/**
	 * @param merchantUniqueReference the merchantUniqueReference to set
	 */
	public void setMerchantUniqueReference(String merchantUniqueReference) {
		this.merchantUniqueReference = merchantUniqueReference;
	}

	public String getCustomMerchantResponse() {
		return customMerchantResponse;
	}

	public void setCustomMerchantResponse(String customMerchantResponse) {
		this.customMerchantResponse = customMerchantResponse;
	}

	public String getCustomChecksumString() {
		return customChecksumString;
	}

	public void setCustomChecksumString(String customChecksumString) {
		this.customChecksumString = customChecksumString;
	}

	public String getBlockedAmount() {
		return blockedAmount;
	}

	public void setBlockedAmount(String blockedAmount) {
		this.blockedAmount = blockedAmount;
	}

	public String getPreAuthId() {
		return preAuthId;
	}

	public void setPreAuthId(String preAuthId) {
		this.preAuthId = preAuthId;
	}

	public String getMaskedCardNo() {
		return maskedCardNo;
	}

	public void setMaskedCardNo(String maskedCardNo) {
		this.maskedCardNo = maskedCardNo;
	}

	public String getCardIndexNo() {
		return cardIndexNo;
	}

	public void setCardIndexNo(String cardIndexNo) {
		this.cardIndexNo = cardIndexNo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TxnStatusResponse [txnId=" + txnId + ", bankTxnId=" + bankTxnId + ", orderId=" + orderId
				+ ", txnAmount=" + txnAmount + ", status=" + status + ", txnType=" + txnType + ", gatewayName="
				+ gatewayName + ", respCode=" + respCode + ", respMsg=" + respMsg + ", bankName=" + bankName + ", mid="
				+ mid + ", paymentMode=" + paymentMode + ", refundAmt=" + refundAmt + ", txnDate=" + txnDate
				+ ", refundId=" + refundId + ", refId=" + refId + ", childTransaction=" + childTransaction + ", subsId="
				+ subsId + ", merchantUniqueReference=" + merchantUniqueReference + ", blockedAmount=" + blockedAmount
				+ ", preAuthId=" + preAuthId + ", maskedCardNo=" + maskedCardNo + ", cardIndexNo=" + cardIndexNo
				+ ", maskedCustomerMobileNumber=" + maskedCustomerMobileNumber + ", posId=" + posId
				+ ", uniqueReferenceLabel=" + uniqueReferenceLabel + ", uniqueReferenceValue=" + uniqueReferenceValue
				+ ", pccCode=" + pccCode + ", prn=" + prn + ", udf1=" + udf1 + ", udf2=" + udf2 + ", udf3=" + udf3
				+ ", comments=" + comments + ", currentTxnCount=" + currentTxnCount + ", loyaltyPoints=" + loyaltyPoints
				+ ", toString()=" + super.toString() + "]";
	}

}
