package com.paytm.pg.response;

import com.paytm.pg.model.ResultInfo;
import com.paytm.pg.response.body.NativeCashierInfoResponseBody;
import com.paytm.pg.response.header.ResponseHeader;

public class NativeCashierInfoResponse {

	private ResponseHeader head;
	private NativeCashierInfoResponseBody body;

	public NativeCashierInfoResponse() {
		super();
	}

	public NativeCashierInfoResponse(ResponseHeader head, NativeCashierInfoResponseBody body) {
		super();
		this.head = head;
		this.body = body;
	}

	public NativeCashierInfoResponse(ResultInfo result) {
		this.head = new ResponseHeader();
		this.body = new NativeCashierInfoResponseBody(result);
	}

	public ResponseHeader getHead() {
		return head;
	}

	public void setHead(ResponseHeader head) {
		this.head = head;
	}

	public NativeCashierInfoResponseBody getBody() {
		return body;
	}

	public void setBody(NativeCashierInfoResponseBody body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "NativeCashierInfoResponse [head=" + head + ", body=" + body + ", toString()=" + super.toString() + "]";
	}

}
