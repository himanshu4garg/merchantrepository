package com.paytm.pg.response.body;

import com.paytm.pg.model.ResultInfo;

public class BaseResponseBody {

	private ResultInfo resultInfo;

	public BaseResponseBody(ResultInfo resultInfo) {
		this.resultInfo = resultInfo;
	}

	public ResultInfo getResultInfo() {
		return resultInfo;
	}

	public void setResultInfo(ResultInfo resultInfo) {
		this.resultInfo = resultInfo;
	}

	@Override
	public String toString() {
		return "BaseResponseBody [resultInfo=" + resultInfo + super.toString() + "]";
	}

}
