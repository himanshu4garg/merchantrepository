package com.paytm.pg.response.body;

import com.paytm.pg.enums.EPayMode;
import com.paytm.pg.model.MerchantDetails;
import com.paytm.pg.model.NativePromoCodeData;
import com.paytm.pg.model.PayOption;
import com.paytm.pg.model.ResultInfo;

public class NativeCashierInfoResponseBody extends BaseResponseBody {

	public NativeCashierInfoResponseBody(ResultInfo resultInfo, EPayMode paymentFlow, PayOption merchantPayOption,
			PayOption addMoneyPayOption, MerchantDetails merchantDetails, MerchantDetails addMoneyMerchantDetails,
			boolean walletOnly, String merchantOfferMessage, NativePromoCodeData promoCodeData) {
		super(resultInfo);
		this.paymentFlow = paymentFlow;
		this.merchantPayOption = merchantPayOption;
		this.addMoneyPayOption = addMoneyPayOption;
		this.merchantDetails = merchantDetails;
		this.addMoneyMerchantDetails = addMoneyMerchantDetails;
		this.walletOnly = walletOnly;
		this.merchantOfferMessage = merchantOfferMessage;
		this.promoCodeData = promoCodeData;
	}

	public NativeCashierInfoResponseBody(ResultInfo result) {
		super(result);
	}

	private EPayMode paymentFlow;
	private PayOption merchantPayOption;
	private PayOption addMoneyPayOption;
	private MerchantDetails merchantDetails;
	private MerchantDetails addMoneyMerchantDetails;
	private boolean walletOnly;
	private String merchantOfferMessage;
	private NativePromoCodeData promoCodeData;

	public EPayMode getPaymentFlow() {
		return paymentFlow;
	}

	public void setPaymentFlow(EPayMode paymentFlow) {
		this.paymentFlow = paymentFlow;
	}

	public PayOption getMerchantPayOption() {
		return merchantPayOption;
	}

	public void setMerchantPayOption(PayOption merchantPayOption) {
		this.merchantPayOption = merchantPayOption;
	}

	public PayOption getAddMoneyPayOption() {
		return addMoneyPayOption;
	}

	public void setAddMoneyPayOption(PayOption addMoneyPayOption) {
		this.addMoneyPayOption = addMoneyPayOption;
	}

	public MerchantDetails getMerchantDetails() {
		return merchantDetails;
	}

	public void setMerchantDetails(MerchantDetails merchantDetails) {
		this.merchantDetails = merchantDetails;
	}

	public MerchantDetails getAddMoneyMerchantDetails() {
		return addMoneyMerchantDetails;
	}

	public void setAddMoneyMerchantDetails(MerchantDetails addMoneyMerchantDetails) {
		this.addMoneyMerchantDetails = addMoneyMerchantDetails;
	}

	public boolean isWalletOnly() {
		return walletOnly;
	}

	public void setWalletOnly(boolean walletOnly) {
		this.walletOnly = walletOnly;
	}

	public void setMerchantOfferMessage(String merchantOfferMessage) {
		this.merchantOfferMessage = merchantOfferMessage;
	}

	public NativePromoCodeData getPromoCodeData() {
		return promoCodeData;
	}

	public void setPromoCodeData(NativePromoCodeData promoCodeData) {
		this.promoCodeData = promoCodeData;
	}

	@Override
	public String toString() {
		return "NativeCashierInfoResponseBody [paymentFlow=" + paymentFlow + ", merchantPayOption=" + merchantPayOption
				+ ", addMoneyPayOption=" + addMoneyPayOption + ", merchantDetails=" + merchantDetails
				+ ", addMoneyMerchantDetails=" + addMoneyMerchantDetails + ", walletOnly=" + walletOnly
				+ ", merchantOfferMessage=" + merchantOfferMessage + ", promoCodeData=" + promoCodeData
				+ super.toString() + "]";
	}

}
