package com.paytm.pg.response.body;

import com.paytm.pg.model.ResultInfo;

public class InitiateTransactionResponseBody extends BaseResponseBody {
	private String txnToken;
	private boolean isPromoCodeValid;
	private String subscriptionId;
	private boolean authenticated;
	private String callbackUrl;

	public InitiateTransactionResponseBody(ResultInfo resultInfo, String txnToken, boolean isAuthenticated) {
		super(resultInfo);
		this.txnToken = txnToken;
		this.authenticated = isAuthenticated;
	}

	public InitiateTransactionResponseBody(ResultInfo resultInfo, String txnToken, boolean isAuthenticated,
			String callbackUrl) {
		super(resultInfo);
		this.txnToken = txnToken;
		this.authenticated = isAuthenticated;
		this.callbackUrl = callbackUrl;
	}

	public InitiateTransactionResponseBody(ResultInfo resultInfo, String txnToken, boolean isPromoCodeValid,
			String subscriptionId, boolean isAuthenticated, String callbackUrl) {
		super(resultInfo);
		this.txnToken = txnToken;
		this.isPromoCodeValid = isPromoCodeValid;
		this.subscriptionId = subscriptionId;
		this.authenticated = isAuthenticated;
		this.callbackUrl = callbackUrl;
	}

	public InitiateTransactionResponseBody(ResultInfo result) {
		super(result);
	}

	public String getTxnToken() {
		return txnToken;
	}

	public void setTxnToken(String txnToken) {
		this.txnToken = txnToken;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public boolean isPromoCodeValid() {
		return isPromoCodeValid;
	}

	public void setPromoCodeValid(boolean promoCodeValid) {
		isPromoCodeValid = promoCodeValid;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	@Override
	public String toString() {
		return "InitiateTransactionResponseBody [txnToken=" + txnToken + ", isPromoCodeValid=" + isPromoCodeValid
				+ ", subscriptionId=" + subscriptionId + ", authenticated=" + authenticated + ", callbackUrl="
				+ callbackUrl + super.toString() + "]";
	}

}
