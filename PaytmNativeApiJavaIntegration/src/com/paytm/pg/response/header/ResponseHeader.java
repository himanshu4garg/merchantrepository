package com.paytm.pg.response.header;

public class ResponseHeader {

	public String responseTimestamp;
	public String version;

	public ResponseHeader() {
		super();
		this.responseTimestamp = String.valueOf(System.currentTimeMillis());
		this.version = "v1";
	}

	public ResponseHeader(String version) {
		super();
		this.responseTimestamp = String.valueOf(System.currentTimeMillis());
		this.version = version;
	}

	public String getResponseTimestamp() {
		return responseTimestamp;
	}

	public void setResponseTimestamp(String responseTimestamp) {
		this.responseTimestamp = responseTimestamp;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "ResponseHeader [responseTimestamp=" + responseTimestamp + ", version=" + version + super.toString()
				+ "]";
	}

}
