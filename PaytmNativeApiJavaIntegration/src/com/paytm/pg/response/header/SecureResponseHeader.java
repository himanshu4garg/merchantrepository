package com.paytm.pg.response.header;

public class SecureResponseHeader extends ResponseHeader {
	public String clientId;
	public String signature;

	public SecureResponseHeader(String clientId, String signature) {
		super();
		this.clientId = clientId;
		this.signature = signature;
	}

	public SecureResponseHeader() {
		super();
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return "SecureResponseHeader [clientId=" + clientId + ", signature=" + signature + super.toString() + "]";
	}

}
