package com.paytm.pg.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CreateConnectionAndCall {

	public static String hitUrl(String postData, String callUrl) {
		String responseData = "";
		try {
			URL url = new URL(callUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setUseCaches(false);
			connection.setDoOutput(true);

			DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
			requestWriter.writeBytes(postData);
			requestWriter.close();
			InputStream is;

			is = connection.getInputStream();

			BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));

			if ((responseData = responseReader.readLine()) != null) {
				System.out.append("Response Json = " + responseData);
			}
			responseReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseData;
	}

}
