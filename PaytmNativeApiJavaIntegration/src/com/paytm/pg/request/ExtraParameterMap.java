package com.paytm.pg.request;

import java.util.Map;

public class ExtraParameterMap {
	private Map<String, Object> extraParamsMap;

	protected ExtraParameterMap() {
	}

	public ExtraParameterMap(Map<String, Object> extraParamsMap) {
		this.extraParamsMap = extraParamsMap;
	}

	public Map<String, Object> getExtraParamsMap() {
		return extraParamsMap;
	}

	public void setExtraParamsMap(Map<String, Object> extraParamsMap) {
		this.extraParamsMap = extraParamsMap;
	}

	@Override
	public String toString() {
		return "ExtraParameterMap [extraParamsMap=" + extraParamsMap + ", toString()=" + super.toString() + "]";
	}

}
