package com.paytm.pg.request.body;

import java.util.List;

import com.paytm.pg.model.ExtendInfo;
import com.paytm.pg.model.GoodsInfo;
import com.paytm.pg.model.Money;
import com.paytm.pg.model.PaymentMode;
import com.paytm.pg.model.ShippingInfo;
import com.paytm.pg.model.UserInfo;

public class InitiateTransactionRequestBody {

	private String requestType;
	private String mid;
	private String orderId;
	private String websiteName;
	private Money txnAmount;
	private UserInfo userInfo;
	private String paytmSsoToken;
	private List<PaymentMode> enablePaymentMode = null;
	private List<PaymentMode> disablePaymentMode = null;
	private String promoCode;
	private String callbackUrl;
	private List<GoodsInfo> goods = null;
	private List<ShippingInfo> shippingInfo = null;
	private ExtendInfo extendInfo;
	private String subsPPIOnly;
	private String subsPaymentMode;
	private String subscriptionAmountType;
	private String subscriptionMaxAmount;
	private String subscriptionFrequency;
	private String subscriptionFrequencyUnit;
	private String subscriptionExpiryDate;
	private String subscriptionEnableRetry;
	private String subscriptionGraceDays;
	private String subscriptionStartDate;
	private String subscriptionRetryCount;
	private String emiOption;
	private String cardTokenRequired;
	private boolean validateCardIndex;

	public InitiateTransactionRequestBody(String requestType, String mid, String orderId, String websiteName,
			Money txnAmount, UserInfo userInfo, String paytmSsoToken, List<PaymentMode> enablePaymentMode,
			List<PaymentMode> disablePaymentMode, String promoCode, String callbackUrl, List<GoodsInfo> goods,
			List<ShippingInfo> shippingInfo, ExtendInfo extendInfo, String subsPPIOnly, String subsPaymentMode,
			String subscriptionAmountType, String subscriptionMaxAmount, String subscriptionFrequency,
			String subscriptionFrequencyUnit, String subscriptionExpiryDate, String subscriptionEnableRetry,
			String subscriptionGraceDays, String subscriptionStartDate, String subscriptionRetryCount, String emiOption,
			String cardTokenRequired, boolean validateCardIndex) {
		super();
		this.requestType = requestType;
		this.mid = mid;
		this.orderId = orderId;
		this.websiteName = websiteName;
		this.txnAmount = txnAmount;
		this.userInfo = userInfo;
		this.paytmSsoToken = paytmSsoToken;
		this.enablePaymentMode = enablePaymentMode;
		this.disablePaymentMode = disablePaymentMode;
		this.promoCode = promoCode;
		this.callbackUrl = callbackUrl;
		this.goods = goods;
		this.shippingInfo = shippingInfo;
		this.extendInfo = extendInfo;
		this.subsPPIOnly = subsPPIOnly;
		this.subsPaymentMode = subsPaymentMode;
		this.subscriptionAmountType = subscriptionAmountType;
		this.subscriptionMaxAmount = subscriptionMaxAmount;
		this.subscriptionFrequency = subscriptionFrequency;
		this.subscriptionFrequencyUnit = subscriptionFrequencyUnit;
		this.subscriptionExpiryDate = subscriptionExpiryDate;
		this.subscriptionEnableRetry = subscriptionEnableRetry;
		this.subscriptionGraceDays = subscriptionGraceDays;
		this.subscriptionStartDate = subscriptionStartDate;
		this.subscriptionRetryCount = subscriptionRetryCount;
		this.emiOption = emiOption;
		this.cardTokenRequired = cardTokenRequired;
		this.validateCardIndex = validateCardIndex;
	}

	public String getEmiOption() {
		return emiOption;
	}

	public void setEmiOption(String emiOption) {
		this.emiOption = emiOption;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getWebsiteName() {
		return websiteName;
	}

	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}

	public Money getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(Money txnAmount) {
		this.txnAmount = txnAmount;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public String getPaytmSsoToken() {
		return paytmSsoToken;
	}

	public void setPaytmSsoToken(String paytmSsoToken) {
		this.paytmSsoToken = paytmSsoToken;
	}

	public List<PaymentMode> getEnablePaymentMode() {
		return enablePaymentMode;
	}

	public void setEnablePaymentMode(List<PaymentMode> enablePaymentMode) {
		this.enablePaymentMode = enablePaymentMode;
	}

	public List<PaymentMode> getDisablePaymentMode() {
		return disablePaymentMode;
	}

	public void setDisablePaymentMode(List<PaymentMode> disablePaymentMode) {
		this.disablePaymentMode = disablePaymentMode;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public List<GoodsInfo> getGoods() {
		return goods;
	}

	public void setGoods(List<GoodsInfo> goods) {
		this.goods = goods;
	}

	public List<ShippingInfo> getShippingInfo() {
		return shippingInfo;
	}

	public void setShippingInfo(List<ShippingInfo> shippingInfo) {
		this.shippingInfo = shippingInfo;
	}

	public ExtendInfo getExtendInfo() {
		return extendInfo;
	}

	public void setExtendInfo(ExtendInfo extendInfo) {
		this.extendInfo = extendInfo;
	}

	public String getSubsPPIOnly() {
		return subsPPIOnly;
	}

	public void setSubsPPIOnly(String subsPPIOnly) {
		this.subsPPIOnly = subsPPIOnly;
	}

	public String getSubsPaymentMode() {
		return subsPaymentMode;
	}

	public void setSubsPaymentMode(String subsPaymentMode) {
		this.subsPaymentMode = subsPaymentMode;
	}

	public String getSubscriptionAmountType() {
		return subscriptionAmountType;
	}

	public void setSubscriptionAmountType(String subscriptionAmountType) {
		this.subscriptionAmountType = subscriptionAmountType;
	}

	public String getSubscriptionMaxAmount() {
		return subscriptionMaxAmount;
	}

	public void setSubscriptionMaxAmount(String subscriptionMaxAmount) {
		this.subscriptionMaxAmount = subscriptionMaxAmount;
	}

	public String getSubscriptionFrequency() {
		return subscriptionFrequency;
	}

	public void setSubscriptionFrequency(String subscriptionFrequency) {
		this.subscriptionFrequency = subscriptionFrequency;
	}

	public String getSubscriptionFrequencyUnit() {
		return subscriptionFrequencyUnit;
	}

	public void setSubscriptionFrequencyUnit(String subscriptionFrequencyUnit) {
		this.subscriptionFrequencyUnit = subscriptionFrequencyUnit;
	}

	public String getSubscriptionExpiryDate() {
		return subscriptionExpiryDate;
	}

	public void setSubscriptionExpiryDate(String subscriptionExpiryDate) {
		this.subscriptionExpiryDate = subscriptionExpiryDate;
	}

	public String getSubscriptionEnableRetry() {
		return subscriptionEnableRetry;
	}

	public void setSubscriptionEnableRetry(String subscriptionEnableRetry) {
		this.subscriptionEnableRetry = subscriptionEnableRetry;
	}

	public String getSubscriptionGraceDays() {
		return subscriptionGraceDays;
	}

	public void setSubscriptionGraceDays(String subscriptionGraceDays) {
		this.subscriptionGraceDays = subscriptionGraceDays;
	}

	public String getSubscriptionStartDate() {
		return subscriptionStartDate;
	}

	public void setSubscriptionStartDate(String subscriptionStartDate) {
		this.subscriptionStartDate = subscriptionStartDate;
	}

	public String getSubscriptionRetryCount() {
		return subscriptionRetryCount;
	}

	public void setSubscriptionRetryCount(String subscriptionRetryCount) {
		this.subscriptionRetryCount = subscriptionRetryCount;
	}

	public String getCardTokenRequired() {
		return cardTokenRequired;
	}

	public void setCardTokenRequired(String cardTokenRequired) {
		this.cardTokenRequired = cardTokenRequired;
	}

	public boolean isValidateCardIndex() {
		return validateCardIndex;
	}

	public void setValidateCardIndex(boolean validateCardIndex) {
		this.validateCardIndex = validateCardIndex;
	}

	@Override
	public String toString() {
		return "InitiateTransactionRequestBody [requestType=" + requestType + ", mid=" + mid + ", orderId=" + orderId
				+ ", websiteName=" + websiteName + ", txnAmount=" + txnAmount + ", userInfo=" + userInfo
				+ ", paytmSsoToken=" + paytmSsoToken + ", enablePaymentMode=" + enablePaymentMode
				+ ", disablePaymentMode=" + disablePaymentMode + ", promoCode=" + promoCode + ", callbackUrl="
				+ callbackUrl + ", goods=" + goods + ", shippingInfo=" + shippingInfo + ", extendInfo=" + extendInfo
				+ ", subsPPIOnly=" + subsPPIOnly + ", subsPaymentMode=" + subsPaymentMode + ", subscriptionAmountType="
				+ subscriptionAmountType + ", subscriptionMaxAmount=" + subscriptionMaxAmount
				+ ", subscriptionFrequency=" + subscriptionFrequency + ", subscriptionFrequencyUnit="
				+ subscriptionFrequencyUnit + ", subscriptionExpiryDate=" + subscriptionExpiryDate
				+ ", subscriptionEnableRetry=" + subscriptionEnableRetry + ", subscriptionGraceDays="
				+ subscriptionGraceDays + ", subscriptionStartDate=" + subscriptionStartDate
				+ ", subscriptionRetryCount=" + subscriptionRetryCount + ", emiOption=" + emiOption
				+ ", cardTokenRequired=" + cardTokenRequired + ", validateCardIndex=" + validateCardIndex
				+ ", toString()=" + super.toString() + "]";
	}

}
