package com.paytm.pg.request.body;

import java.util.List;

import com.paytm.pg.model.PaymentMode;

public class NativeCashierInfoRequestBody {

	private List<PaymentMode> enablePaymentMode = null;
	private List<PaymentMode> disablePaymentMode = null;

	public NativeCashierInfoRequestBody(List<PaymentMode> enablePaymentMode, List<PaymentMode> disablePaymentMode) {
		super();
		this.enablePaymentMode = enablePaymentMode;
		this.disablePaymentMode = disablePaymentMode;
	}

	public List<PaymentMode> getEnablePaymentMode() {
		return enablePaymentMode;
	}

	public void setEnablePaymentMode(List<PaymentMode> enablePaymentMode) {
		this.enablePaymentMode = enablePaymentMode;
	}

	public List<PaymentMode> getDisablePaymentMode() {
		return disablePaymentMode;
	}

	public void setDisablePaymentMode(List<PaymentMode> disablePaymentMode) {
		this.disablePaymentMode = disablePaymentMode;
	}

	@Override
	public String toString() {
		return "NativeCashierInfoRequestBody [enablePaymentMode=" + enablePaymentMode + ", disablePaymentMode="
				+ disablePaymentMode + ", toString()=" + super.toString() + "]";
	}

}
