package com.paytm.pg.request;

import com.google.gson.annotations.SerializedName;

public class TxnStatusBaseRequest extends ExtraParameterMap {

	public static final String MID = "MID";
	public static final String ORDERID = "ORDERID";
	public static final String TXNTYPE = "TXNTYPE";

	@SerializedName(MID)
	private String mid;

	@SerializedName(ORDERID)
	private String orderId;

	@SerializedName(TXNTYPE)
	private String txnType;

	private String customCheckSumString;

	private boolean fromAoaMerchant;

	public TxnStatusBaseRequest() {
	}

	public TxnStatusBaseRequest(String mid, String orderId, String txnType) {
		this.mid = mid;
		this.orderId = orderId;
		this.txnType = txnType;
	}

	public TxnStatusBaseRequest(String mid, String orderId, String txnType, String customCheckSumString,
			boolean fromAoaMerchant) {
		super();
		this.mid = mid;
		this.orderId = orderId;
		this.txnType = txnType;
		this.customCheckSumString = customCheckSumString;
		this.fromAoaMerchant = fromAoaMerchant;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCustomCheckSumString() {
		return customCheckSumString;
	}

	public void setCustomCheckSumString(String customCheckSumString) {
		this.customCheckSumString = customCheckSumString;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public boolean isFromAoaMerchant() {
		return fromAoaMerchant;
	}

	public void setFromAoaMerchant(boolean fromAoaMerchant) {
		this.fromAoaMerchant = fromAoaMerchant;
	}

	@Override
	public String toString() {
		return "TxnStatusBaseRequest [mid=" + mid + ", orderId=" + orderId + ", txnType=" + txnType
				+ ", customCheckSumString=" + customCheckSumString + ", fromAoaMerchant=" + fromAoaMerchant
				+ ", toString()=" + super.toString() + "]";
	}

}
