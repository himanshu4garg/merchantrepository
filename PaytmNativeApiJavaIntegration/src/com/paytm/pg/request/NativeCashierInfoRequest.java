package com.paytm.pg.request;

import com.paytm.pg.request.body.NativeCashierInfoRequestBody;
import com.paytm.pg.request.header.TokenRequestHeader;

public class NativeCashierInfoRequest {
	private TokenRequestHeader head;

	private NativeCashierInfoRequestBody body;

	public NativeCashierInfoRequest(TokenRequestHeader head, NativeCashierInfoRequestBody body) {
		super();
		this.head = head;
		this.body = body;
	}

	public TokenRequestHeader getHead() {
		return head;
	}

	public void setHead(TokenRequestHeader head) {
		this.head = head;
	}

	public NativeCashierInfoRequestBody getBody() {
		return body;
	}

	public void setBody(NativeCashierInfoRequestBody body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "NativeCashierInfoRequest [head=" + head + ", body=" + body + ", toString()=" + super.toString() + "]";
	}

}
