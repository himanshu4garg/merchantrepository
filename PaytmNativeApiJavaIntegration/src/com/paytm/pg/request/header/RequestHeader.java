package com.paytm.pg.request.header;

import com.paytm.pg.enums.EChannelId;

public class RequestHeader extends BaseHeader {

	public String requestTimestamp;
	private String workFlow;

	public RequestHeader() {

	}

	public RequestHeader(String version, EChannelId channelId, String requestTimeStamp) {
		super(version, channelId);
		this.requestTimestamp = requestTimeStamp;
	}

	public String getRequestTimestamp() {
		return requestTimestamp;
	}

	public void setRequestTimestamp(String requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public String getWorkFlow() {
		return workFlow;
	}

	public void setWorkFlow(String workFlow) {
		this.workFlow = workFlow;
	}

	@Override
	public String toString() {
		return "RequestHeader [requestTimestamp=" + requestTimestamp + ", workFlow=" + workFlow + super.toString()
				+ "]";
	}

}