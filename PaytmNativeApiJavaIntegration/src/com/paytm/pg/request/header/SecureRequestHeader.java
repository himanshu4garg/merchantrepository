package com.paytm.pg.request.header;

import com.paytm.pg.enums.EChannelId;

public class SecureRequestHeader extends RequestHeader {

	private String clientId;
	private String signature;


	public SecureRequestHeader(String version, EChannelId channelId, String requestTimeStamp, String clientId,
			String signature) {
		super(version, channelId, requestTimeStamp);
		this.clientId = clientId;
		this.signature = signature;	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return "SecureRequestHeader [clientId=" + clientId + ", signature=" + signature + super.toString() + "]";
	}

}