package com.paytm.pg.request.header;

import com.paytm.pg.enums.EChannelId;

public class BaseHeader {

	public String version;
	public EChannelId channelId;

	public BaseHeader() {

	}

	public BaseHeader(String version, EChannelId channelId) {
		super();
		this.channelId = channelId;
		this.version = version;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public EChannelId getChannelId() {
		return channelId;
	}

	public void setChannelId(EChannelId channelId) {
		this.channelId = channelId;
	}

	@Override
	public String toString() {
		return "BaseHeader [version=" + version + ", channelId=" + channelId + ", toString()=" + super.toString() + "]";
	}

}
