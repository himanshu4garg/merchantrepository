package com.paytm.pg.request.header;

import com.paytm.pg.enums.EChannelId;

public class TokenRequestHeader extends RequestHeader {

	private String txnToken;

	public TokenRequestHeader(String txnToken, String requestTimestamp, String version,
			EChannelId channelId) {
		super(version, channelId, requestTimestamp);
		this.txnToken = txnToken;
	}

	public String getTxnToken() {
		return txnToken;
	}

	public void setTxnToken(String txnToken) {
		this.txnToken = txnToken;
	}

	@Override
	public String toString() {
		return "TokenRequestHeader [txnToken=" + txnToken + ", toString()=" + super.toString() + "]";
	}

}
