package com.paytm.pg.request;

import com.paytm.pg.request.body.InitiateTransactionRequestBody;
import com.paytm.pg.request.header.SecureRequestHeader;

public class InitiateTransactionRequest {

	private SecureRequestHeader head;

	private InitiateTransactionRequestBody body;

	public InitiateTransactionRequest(SecureRequestHeader head, InitiateTransactionRequestBody body) {
		this.head = head;
		this.body = body;
	}

	public SecureRequestHeader getHead() {
		return head;
	}

	public void setHead(SecureRequestHeader head) {
		this.head = head;
	}

	public InitiateTransactionRequestBody getBody() {
		return body;
	}

	public void setBody(InitiateTransactionRequestBody body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "InitiateTransactionRequest [head=" + head + ", body=" + body + ", toString()=" + super.toString() + "]";
	}

}
