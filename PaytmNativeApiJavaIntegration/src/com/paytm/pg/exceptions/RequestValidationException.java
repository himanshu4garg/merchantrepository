package com.paytm.pg.exceptions;

import com.merchant.call.ErrorConstants;

public class RequestValidationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RequestValidationException(String msg) {
		super(msg);
	}

	public static RequestValidationException getMissingMandatoryParametersException() {
		return new RequestValidationException(ErrorConstants.ErrorMessage.MISSING_MANDATORY_PARAMETERS);
	}

	public static RequestValidationException getTransactionTokenException() {
		return new RequestValidationException(ErrorConstants.ErrorMessage.MISSING_TRANSACTOKEN_TOKEN);
	}

}
