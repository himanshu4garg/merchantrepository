package com.paytm.pg.enums;

public enum EPayMode {

	NONE("NONE"), ADDANDPAY("ADDANDPAY"), HYBRID("HYBRID"), ADDANDPAY_KYC("KYC"), NONE_KYC("NONE_KYC");

	private String value;

	EPayMode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}