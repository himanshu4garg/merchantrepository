package com.paytm.pg.enums;

public enum EChannelId {
	APP("APP"), WEB("WEB"), WAP("WAP"), SYSTEM("SYSTEM");

	private String value;

	EChannelId(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
