package com.merchant.webhook;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.merchant.call.ObjectToJson;
import com.paytm.pg.model.ResultInfo;

public class ResponceFromPaytmPG extends HttpServlet {
	public ResponceFromPaytmPG() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.getWriter().append("Served at::").append(request.getContextPath());
		System.out.println("get...");
		/*
		 * here merchant will complete his requirement
		 * 
		 */

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		ResultInfo result = new ResultInfo("success", "201", "message received");
		String postData = ObjectToJson.changeObjectToJsonString(result);
		out.print(postData);
		out.flush();
	}

	@SuppressWarnings("deprecation")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.getWriter().append("Served at::").append(request.getContextPath());
		System.out.println("called...");
		/*
		 * here merchant will complete his requirement
		 * 
		 */

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		ResultInfo result = new ResultInfo("success", "201", "message received");
		String postData = ObjectToJson.changeObjectToJsonString(result);
		out.print(postData);
		out.flush();
	}
}
