package com.merchant.call;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.paytm.pg.response.InitiateTransactionResponse;
import com.paytm.pg.response.NativeCashierInfoResponse;
import com.paytm.pg.response.TxnStatusResponse;

public class MerchantInitiateTransaction extends HttpServlet {
	public MerchantInitiateTransaction() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.getWriter().append("Served at::").append(request.getContextPath());
		System.out.println("get...");
	}

	@SuppressWarnings("deprecation")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.getWriter().append("Served at::").append(request.getContextPath());

		InitiateTransactionResponse initiateTransactionResponse = Test.initiateTransaction();

		NativeCashierInfoResponse fetchPaymentOptionsResponse = Test.fetchPaymentOptions(initiateTransactionResponse);

		TxnStatusResponse merchantStatusResponse = Test.merchantStatus();

		System.out.println("called...");

	}
}
