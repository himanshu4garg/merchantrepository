package com.merchant.call;

public class ErrorConstants {
	public final static String FAILURE = "failure";

	public static class ErrorMessage{
		public final static String MISSING_MANDATORY_PARAMETERS = "missing mandatory parameters!!!";
		public final static String MISSING_TRANSACTOKEN_TOKEN = "missing transaction token!!!";
	}

	public static class ErrorCode{
		public final static String MISSING_MANDATORY_PARAMETERS = "422";
		public final static String MISSING_TRANSACTOKEN_TOKEN = "423";
	}
}
