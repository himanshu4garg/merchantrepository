package com.merchant.call;

import com.google.gson.Gson;

public class ObjectToJson {

	public static String changeObjectToJsonString(Object request) {
		return new Gson().toJson(request);
	}
}
