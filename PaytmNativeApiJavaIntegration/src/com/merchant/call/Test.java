package com.merchant.call;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.merchant.apiCall.FetchTransactionOptions;
import com.merchant.apiCall.InitiateTransaction;
import com.merchant.apiCall.MerchantStatus;
import com.paytm.pg.enums.EChannelId;
import com.paytm.pg.model.ExtendInfo;
import com.paytm.pg.model.GoodsInfo;
import com.paytm.pg.model.Money;
import com.paytm.pg.model.PaymentMode;
import com.paytm.pg.model.ShippingInfo;
import com.paytm.pg.model.UserInfo;
import com.paytm.pg.request.InitiateTransactionRequest;
import com.paytm.pg.request.NativeCashierInfoRequest;
import com.paytm.pg.request.TxnStatusBaseRequest;
import com.paytm.pg.request.body.InitiateTransactionRequestBody;
import com.paytm.pg.request.body.NativeCashierInfoRequestBody;
import com.paytm.pg.request.header.SecureRequestHeader;
import com.paytm.pg.request.header.TokenRequestHeader;
import com.paytm.pg.response.InitiateTransactionResponse;
import com.paytm.pg.response.NativeCashierInfoResponse;
import com.paytm.pg.response.TxnStatusResponse;

public class Test {

	public static void main(String[] args) {

		/*
		 * request creation for InitiateTransaction and call function for request head
		 * and body are created
		 */
		String version = "v1";
		String requestTimeStamp = "Time";
		String clientId = "C11";
		String signature = "CH";
		EChannelId channelId = EChannelId.WEB;

		String requestType = "Payment";
		String mid = MerchantConstants.MID;
		String orderId = "PARCEL892121222834";
		String websiteName = MerchantConstants.WEBSITE;
		Money txnAmount = new Money("INR", "1.00");
		UserInfo userInfo = new UserInfo("cid", "", "", "", "");
		String paytmSsoToken = "";
		List<PaymentMode> enablePaymentMode = null;

		List<PaymentMode> disablePaymentMode = null;

		String promoCode = "";
		String callbackUrl = MerchantConstants.CALLBACK_URL;
		List<GoodsInfo> goods = null;
		List<ShippingInfo> shippingInfo = null;
		String amountToBeRefunded = "1.00";
		Map<String, String> subwalletAmount = new TreeMap<>();
		subwalletAmount.put("FOOD", "2");
		subwalletAmount.put("GIFT", "2.5");
		ExtendInfo extendInfo = new ExtendInfo("udf1", "udf2", "udf3", "mercUnqRef", "comment", amountToBeRefunded,
				subwalletAmount);
		String subsPPIOnly = null;
		String subsPaymentMode = null;
		String subscriptionAmountType = null;
		String subscriptionMaxAmount = null;
		String subscriptionFrequency = null;
		String subscriptionFrequencyUnit = null;
		String subscriptionExpiryDate = null;
		String subscriptionEnableRetry = null;
		String subscriptionGraceDays = null;
		String subscriptionStartDate = null;
		String subscriptionRetryCount = null;
		String emiOption = null;
		String cardTokenRequired = null;
		boolean validateCardIndex = false;

		SecureRequestHeader head = new SecureRequestHeader(version, channelId, requestTimeStamp, clientId, signature);

		InitiateTransactionRequestBody body = new InitiateTransactionRequestBody(requestType, mid, orderId, websiteName,
				txnAmount, userInfo, paytmSsoToken, enablePaymentMode, disablePaymentMode, promoCode, callbackUrl,
				goods, shippingInfo, extendInfo, subsPPIOnly, subsPaymentMode, subscriptionAmountType,
				subscriptionMaxAmount, subscriptionFrequency, subscriptionFrequencyUnit, subscriptionExpiryDate,
				subscriptionEnableRetry, subscriptionGraceDays, subscriptionStartDate, subscriptionRetryCount,
				emiOption, cardTokenRequired, validateCardIndex);

		InitiateTransactionRequest request = new InitiateTransactionRequest(head, body);
		InitiateTransactionResponse response = InitiateTransaction.call(request);
		System.out.println("InitiateTransactionResponse " + response);
		fetchPaymentOptions(response);
	}

	public static InitiateTransactionResponse initiateTransaction() {
		/*
		 * request creation for InitiateTransaction and call function for request head
		 * and body are created
		 */
		String version = "v1";
		String requestTimeStamp = "requestTimeStamp";
		String clientId = "C11";
		String signature = "CH";

		String requestType = "Payment";
		String mid = MerchantConstants.MID;
		String orderId = "PARCEL892121222834";
		String websiteName = "W";
		Money txnAmount = new Money("INR", "1.00");
		UserInfo userInfo = new UserInfo("cid", "", "", "", "");
		String paytmSsoToken = "paytmSsoToken";
		List<PaymentMode> enablePaymentMode = null;
		List<PaymentMode> disablePaymentMode = null;
		String promoCode = "";
		String callbackUrl = MerchantConstants.CALLBACK_URL;
		List<GoodsInfo> goods = null;
		List<ShippingInfo> shippingInfo = null;
		String amountToBeRefunded = "1.00";
		Map<String, String> subwalletAmount = new TreeMap<>();
		subwalletAmount.put("FOOD", "2");
		subwalletAmount.put("GIFT", "2.5");
		ExtendInfo extendInfo = new ExtendInfo("udf1", "udf2", "udf3", "mercUnqRef", "comment", amountToBeRefunded,
				subwalletAmount);
		String subsPPIOnly = null;
		String subsPaymentMode = null;
		String subscriptionAmountType = null;
		String subscriptionMaxAmount = null;
		String subscriptionFrequency = null;
		String subscriptionFrequencyUnit = null;
		String subscriptionExpiryDate = null;
		String subscriptionEnableRetry = null;
		String subscriptionGraceDays = null;
		String subscriptionStartDate = null;
		String subscriptionRetryCount = null;
		String emiOption = null;
		String cardTokenRequired = null;
		boolean validateCardIndex = false;

		SecureRequestHeader head = new SecureRequestHeader(version, EChannelId.WEB, requestTimeStamp, clientId,
				signature);
		InitiateTransactionRequestBody body = new InitiateTransactionRequestBody(requestType, mid, orderId, websiteName,
				txnAmount, userInfo, paytmSsoToken, enablePaymentMode, disablePaymentMode, promoCode, callbackUrl,
				goods, shippingInfo, extendInfo, subsPPIOnly, subsPaymentMode, subscriptionAmountType,
				subscriptionMaxAmount, subscriptionFrequency, subscriptionFrequencyUnit, subscriptionExpiryDate,
				subscriptionEnableRetry, subscriptionGraceDays, subscriptionStartDate, subscriptionRetryCount,
				emiOption, cardTokenRequired, validateCardIndex);

		InitiateTransactionRequest request = new InitiateTransactionRequest(head, body);

		// System.out.println("himanshu");
		InitiateTransactionResponse response = InitiateTransaction.call(request);

		System.out.println("InitiateTransactionResponse " + response);
		return response;

	}

	public static NativeCashierInfoResponse fetchPaymentOptions(
			InitiateTransactionResponse initiateTransactionResponse) {

		String txnToken = initiateTransactionResponse.getBody().getTxnToken();
		String requestTimestamp = "requestTimeStamp";
		String version = "v1";
		EChannelId channelId = EChannelId.WEB;

		List<PaymentMode> enablePaymentMode = null;
		List<PaymentMode> disablePaymentMode = null;

		TokenRequestHeader head = new TokenRequestHeader(txnToken, requestTimestamp, version, channelId);
		NativeCashierInfoRequestBody body = new NativeCashierInfoRequestBody(enablePaymentMode, disablePaymentMode);
		NativeCashierInfoRequest request = new NativeCashierInfoRequest(head, body);

		NativeCashierInfoResponse response = FetchTransactionOptions.call(request, MerchantConstants.MID,
				"PARCEL892121222834");
		System.out.println("NativeCashierInfoResponse  " + response);
		return response;
	}

	public static TxnStatusResponse merchantStatus() {

		String mid = MerchantConstants.MID;
		String orderId = "PARCEL892121222834";
		String txnType = "Payment";
		String customCheckSumString = "CH";
		boolean fromAoaMerchant = false;

		TxnStatusBaseRequest request = new TxnStatusBaseRequest(mid, orderId, txnType, customCheckSumString,
				fromAoaMerchant);

		TxnStatusResponse response = MerchantStatus.call(request);
		System.out.println("TxnStatusResponse  " + response);

		return response;
	}

}
