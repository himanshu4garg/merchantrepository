package com.merchant.call;

public class MerchantConstants {

	public final static String MID = "PGP31899144818088675";
	public final static String MERCHANT_KEY = "kbzk1DSbJiV_O3p5";
	public final static String WEBSITE = "W";

	public final static String CALLBACK_URL = "https://pg-staging.paytm.in/MerchantSite/bankResponse";
	/*
	 * public final static String MID = "TIMESI54326042593468"; public final static
	 * String MERCHANT_KEY ="%BhQSi7MOy0wt#fs"; public final static String WEBSITE =
	 * "dineoutWEB";
	 */

	// Testing URLs
//	public final static String INITIATE_TXN_URL = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction";
//	public final static String FETCH_PAYMENT_OPTIONS_URL = "https://securegw-stage.paytm.in/merchant-status/getTxnStatus";

	public final static String INITIATE_TXN_URL = "http://localhost:8080/theia/api/v1/initiateTransaction";

	public final static String FETCH_PAYMENT_OPTIONS_URL = "http://localhost:8080/theia/api/v1/fetchPaymentOptions";

	public final static String MERCHANT_STATUS_URL = "https://securegw-stage.paytm.in/merchant-status/getTxnStatus";
	public final static String TXN_STATUS_URL = "https://securegw-stage.paytm.in/merchant-status/getTxnStatus";
	public final static String REFUND_STATUS_URL = "https://securegw-stage.paytm.in/refund/HANDLER_INTERNAL/getRefundStatus";
	public final static String REFUND_URL = "https://securegw-stage.paytm.in/refund/HANDLER_INTERNAL/REFUND";

	// Dev URLs
	/*
	 * public final static String INITIATE_TXN_URL =
	 * "https://securegw.paytm.in/theia/api/v1/initiateTransaction"; public final
	 * static String FETCH_PAYMENT_OPTIONS_URL =
	 * "https://securegw-stage.paytm.in/merchant-status/getTxnStatus"; public final
	 * static String MERCHANT_STATUS_URL =
	 * "https://securegw-stage.paytm.in/merchant-status/getTxnStatus"; public final
	 * static String TXN_STATUS_URL =
	 * "https://securegw-stage.paytm.in/merchant-status/getTxnStatus"; public final
	 * static String REFUND_STATUS_URL =
	 * "https://securegw-stage.paytm.in/refund/HANDLER_INTERNAL/getRefundStatus";
	 * public final static String REFUND_URL =
	 * "https://securegw-stage.paytm.in/refund/HANDLER_INTERNAL/REFUND";
	 */
}
