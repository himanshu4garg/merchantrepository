package com.merchant.apiCall;

import static com.paytm.pg.utils.StringUtils.isEmpty;

import com.google.gson.Gson;
import com.merchant.call.ErrorConstants;
import com.merchant.call.MerchantConstants;
import com.merchant.call.ObjectToJson;
import com.paytm.pg.exceptions.RequestValidationException;
import com.paytm.pg.model.ResultInfo;
import com.paytm.pg.request.InitiateTransactionRequest;
import com.paytm.pg.response.InitiateTransactionResponse;
import com.paytm.pg.utils.CreateConnectionAndCall;
import com.paytm.pg.utils.UrlUtils;

public class InitiateTransaction {

	public static InitiateTransactionResponse call(InitiateTransactionRequest request) {
		try {
			validate(request);
		} catch (RequestValidationException e) {
			System.out.println(e.getMessage());
			ResultInfo result = new ResultInfo(ErrorConstants.FAILURE,
					ErrorConstants.ErrorCode.MISSING_MANDATORY_PARAMETERS, e.getMessage());
			return new InitiateTransactionResponse(result);
		}

		String postData = ObjectToJson.changeObjectToJsonString(request);
		System.out.append("Requested Json = " + postData + " ");

		String url = UrlUtils.createUrl(MerchantConstants.INITIATE_TXN_URL, request.getBody().getMid(),
				request.getBody().getOrderId());
		System.out.println("Requested url = " + url + " ");

		

		String responseData = CreateConnectionAndCall.hitUrl(postData, url);

		InitiateTransactionResponse response = new Gson().fromJson(responseData, InitiateTransactionResponse.class);

		return response;
	}

	private static void validate(InitiateTransactionRequest request) {
		if (isEmpty(request.getBody().getRequestType().trim()) || isEmpty(request.getBody().getMid().trim())
				|| isEmpty(request.getBody().getOrderId().trim()) || isEmpty(request.getBody().getWebsiteName().trim())
				|| null == request.getBody().getTxnAmount()
				|| isEmpty(request.getBody().getTxnAmount().getValue().trim())
				|| null == request.getBody().getUserInfo()
				|| isEmpty(request.getBody().getUserInfo().getCustId().trim())) {
			throw RequestValidationException.getMissingMandatoryParametersException();
		}
	}
}
