package com.merchant.apiCall;

import static com.paytm.pg.utils.StringUtils.isEmpty;

import com.google.gson.Gson;
import com.merchant.call.ErrorConstants;
import com.merchant.call.MerchantConstants;
import com.merchant.call.ObjectToJson;
import com.paytm.pg.exceptions.RequestValidationException;
import com.paytm.pg.model.ResultInfo;
import com.paytm.pg.request.TxnStatusBaseRequest;
import com.paytm.pg.response.TxnStatusResponse;
import com.paytm.pg.utils.CreateConnectionAndCall;

public class MerchantStatus {

	public static TxnStatusResponse call(TxnStatusBaseRequest request) {

		try {
			validate(request);
		} catch (RequestValidationException e) {
			System.out.println(e.getMessage());
			ResultInfo result = new ResultInfo(ErrorConstants.FAILURE,
					ErrorConstants.ErrorCode.MISSING_MANDATORY_PARAMETERS, e.getMessage());
			return new TxnStatusResponse(result.getResultCode(),result.getResultMsg());
		}
		String postData = ObjectToJson.changeObjectToJsonString(request);
		System.out.append("Requested Json = " + postData + " ");

		String responseData = CreateConnectionAndCall.hitUrl(postData, MerchantConstants.FETCH_PAYMENT_OPTIONS_URL);

		TxnStatusResponse response = new Gson().fromJson(responseData, TxnStatusResponse.class);

		return response;
	}

	private static void validate(TxnStatusBaseRequest request) {
		String mid = request.getMid() != null ? request.getMid().trim() : null;
		String orderId = request.getOrderId() != null ? request.getOrderId().trim() : null;

		if (isEmpty(orderId)) {
			throw RequestValidationException.getMissingMandatoryParametersException();
		}
		if (isEmpty(mid)) {
			throw RequestValidationException.getMissingMandatoryParametersException();
		}
	}

}
