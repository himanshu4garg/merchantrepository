package com.merchant.apiCall;

import com.google.gson.Gson;
import com.merchant.call.ErrorConstants;
import com.merchant.call.MerchantConstants;
import com.merchant.call.ObjectToJson;
import com.paytm.pg.exceptions.RequestValidationException;
import com.paytm.pg.model.ResultInfo;
import com.paytm.pg.request.NativeCashierInfoRequest;
import com.paytm.pg.response.NativeCashierInfoResponse;
import com.paytm.pg.utils.CreateConnectionAndCall;
import com.paytm.pg.utils.UrlUtils;

import static com.paytm.pg.utils.StringUtils.isEmpty;

public class FetchTransactionOptions {

	public static NativeCashierInfoResponse call(NativeCashierInfoRequest request, String mid, String orderId) {
		try {
			validate(request);
		} catch (RequestValidationException e) {
			System.out.println(e.getMessage());
			ResultInfo result = new ResultInfo(ErrorConstants.FAILURE,
					ErrorConstants.ErrorCode.MISSING_TRANSACTOKEN_TOKEN, e.getMessage());
			return new NativeCashierInfoResponse(result);
		}
		String postData = ObjectToJson.changeObjectToJsonString(request);
		System.out.append("Requested Json = " + postData + " ");

		String url = UrlUtils.createUrl(MerchantConstants.FETCH_PAYMENT_OPTIONS_URL, mid, orderId);
		System.out.println("Requested url = " + url + " ");

		String responseData = CreateConnectionAndCall.hitUrl(postData, url);

		NativeCashierInfoResponse response = new Gson().fromJson(responseData, NativeCashierInfoResponse.class);

		return response;
	}

	private static void validate(NativeCashierInfoRequest request) {
		if (isEmpty(request.getHead().getTxnToken().trim())) {
			throw RequestValidationException.getTransactionTokenException();
		}
	}

}
